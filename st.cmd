#!/usr/bin/env iocsh.bash

require(mrfioc2)
require(evr_timestamp_buffer)
require(evr_seq_calc)

epicsEnvSet("SYS", "LabS-Embla:TS")
#epicsEnvSet("PCI_SLOT", "1:0.0")
epicsEnvSet("DEVICE", "EVR-01")
epicsEnvSet("EVR", "$(DEVICE)")
epicsEnvSet("CHIC_SYS", "LabS-Embla:")
epicsEnvSet("CHOP_DRV", "Chop-Drv-01")
epicsEnvSet("CHIC_DEV", "TS-$(DEVICE)")
epicsEnvSet("BUFFSIZE", "100")
epicsEnvSet("MRF_HW_DB", "evr-pcie-300dc-ess.db")
epicsEnvSet("E3_MODULES", "/epics/iocs/e3")
epicsEnvSet("EPICS_CMDS", "/epics/iocs/cmds")
epicsEnvSet("INTREF","")

# Find the PCI bus number for the cards in the crate
#system("$(EPICS_CMDS)/$(IOC)/find_pci_bus_id.bash")
#< "$(EPICS_CMDS)/$(IOC)/pci_bus_id"
system("./find_pci_bus_id.bash")
< "./pci_bus_id"


# Load e3-common
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")
 
mrmEvrSetupPCI("$(EVR)", "$(PCI_BUS_NUM):0.0"))
dbLoadRecords("$(MRF_HW_DB)","EVR=$(EVR),SYS=$(SYS),D=$(DEVICE),FEVT=88.0525,PINITSEQ=0")

dbLoadRecords("evrevent.db","EN=$(SYS)-$(DEVICE):EvtI, OBJ=$(DEVICE), CODE=18, EVNT=18"))
dbLoadRecords("evrevent.db","EN=$(SYS)-$(DEVICE):EvtJ, OBJ=$(DEVICE), CODE=19, EVNT=19"))
dbLoadRecords("evrevent.db","EN=$(SYS)-$(DEVICE):EvtK, OBJ=$(DEVICE), CODE=20, EVNT=20"))

# Load timestamp buffer database
iocshLoad("$(evr_timestamp_buffer_DIR)/evr_timestamp_buffer.iocsh", "CHIC_SYS=$(CHIC_SYS), CHIC_DEV=$(CHIC_DEV), CHOP_DRV=$(CHOP_DRV), SYS=$(SYS), BUFFSIZE=$(BUFFSIZE)")

# Load the sequencer configuration script
iocshLoad("$(evr_seq_calc_DIR)/evr_seq_calc.iocsh", "DEV1=$(CHOP_DRV)01:, DEV2=$(CHOP_DRV)02:, DEV3=$(CHOP_DRV)03:, DEV4=$(CHOP_DRV)04:, SYS_EVRSEQ=$(CHIC_SYS), EVR_EVRSEQ=$(CHIC_DEV):")


# The amount of time which the EVR will wait for the 1PPS event before going into error state.
var(evrMrmTimeNSOverflowThreshold, 1000000)

iocInit()


# Set delay compensation target. This is required even when delay compensation
# is disabled to avoid occasionally corrupting timestamps.
#dbpf $(SYS)-$(DEVICE):DC-Tgt-SP 70
dbpf $(SYS)-$(DEVICE):Ena-Sel "Enabled"

dbpf $(SYS)-$(DEVICE):DC-Tgt-SP 100
dbpf $(SYS)-$(DEVICE):DC-Ena-Sel "Enable"

dbpf $(SYS)-$(DEVICE):Evt-Blink0-SP 14

######## Stand alone EVR code #########

dbpf $(SYS)-$(DEVICE):TimeSrc-Sel 0

$(INTREF=#)dbpf $(SYS)-$(DEVICE):TimeSrc-Sel 2

dbpf $(SYS)-$(DEVICE):Time-I.EVNT 125
dbpf $(SYS)-$(DEVICE):Time-I.INP "@OBJ=$(DEVICE), Code=125)"

dbpf $(SYS)-$(DEVICE):Link-Clk-SP 80 #88.0519 #88.0525
epicsThreadSleep 1

dbpf $(SYS)-$(DEVICE):Link-Clk-SP 88.0519 #88.0525
dbpf $(SYS)-$(DEVICE):Time-Clock-SP 88.0519 #88.0525

# Define 14Hz event by dividing event clock and triggering the sequencer (WORKING)
# Notice that the count is different to compensate for 88.05194802 MHz event clock in standalone mode
dbpf $(SYS)-$(DEVICE):PS0-Div-SP 6289424 # 6289464
dbpf $(SYS)-$(DEVICE):PS1-Div-SP 88051900 # 1 PPS


#Setup of sequence for event code 14 generation
dbpf $(SYS)-$(DEVICE):SoftSeq0-RunMode-Sel 0
dbpf $(SYS)-$(DEVICE):SoftSeq0-TrigSrc-2-Sel "Prescaler 0"
dbpf $(SYS)-$(DEVICE):SoftSeq0-TsResolution-Sel 0 #Ticks
dbpf $(SYS)-$(DEVICE):SoftSeq0-Load-Cmd 1
dbpf $(SYS)-$(DEVICE):SoftSeq0-Enable-Cmd 1
#dbpf $(SYS)-$(DEVICE):SoftSeq0-Commit-Cmd 1


######### INPUTS #########

# Set up of UnivIO 0 as Input. Generate Code 10 locally on rising edge.
dbpf $(SYS)-$(DEVICE):In0-Lvl-Sel "Active High"
dbpf $(SYS)-$(DEVICE):In0-Edge-Sel "Active Rising"
dbpf $(SYS)-$(DEVICE):OutFPUV00-Src-SP 61
dbpf $(SYS)-$(DEVICE):In0-Trig-Ext-Sel "Edge"
dbpf $(SYS)-$(DEVICE):In0-Code-Ext-SP 10
dbpf $(SYS)-$(DEVICE):EvtA-SP.OUT "@OBJ=$(EVR),Code=10"
dbpf $(SYS)-$(DEVICE):EvtA-SP.VAL 10

# Set up of UnivIO 1 as Input. Generate Code 11 locally on rising edge.
dbpf $(SYS)-$(DEVICE):In1-Lvl-Sel "Active High"
dbpf $(SYS)-$(DEVICE):In1-Edge-Sel "Active Rising"
dbpf $(SYS)-$(DEVICE):OutFPUV01-Src-SP 61
dbpf $(SYS)-$(DEVICE):In1-Trig-Ext-Sel "Edge"
#dbpf $(SYS)-$(DEVICE):In1-Code-Ext-SP 125
dbpf $(SYS)-$(DEVICE):In1-Code-Ext-SP 11
#dbpf $(SYS)-$(DEVICE):EvtB-SP.OUT "@OBJ=$(EVR),Code=125"
#dbpf $(SYS)-$(DEVICE):EvtB-SP.VAL 125
dbpf $(SYS)-$(DEVICE):EvtB-SP.OUT "@OBJ=$(EVR),Code=11"
dbpf $(SYS)-$(DEVICE):EvtB-SP.VAL 11


dbpf $(SYS)-$(DEVICE):EvtC-SP.OUT "@OBJ=$(EVR),Code=12"
dbpf $(SYS)-$(DEVICE):EvtC-SP.VAL 12
dbpf $(SYS)-$(DEVICE):EvtD-SP.OUT "@OBJ=$(EVR),Code=13"
dbpf $(SYS)-$(DEVICE):EvtD-SP.VAL 13
dbpf $(SYS)-$(DEVICE):EvtE-SP.OUT "@OBJ=$(EVR),Code=14"
dbpf $(SYS)-$(DEVICE):EvtE-SP.VAL 14
dbpf $(SYS)-$(DEVICE):EvtF-SP.OUT "@OBJ=$(EVR),Code=15"
dbpf $(SYS)-$(DEVICE):EvtF-SP.VAL 15
dbpf $(SYS)-$(DEVICE):EvtG-SP.OUT "@OBJ=$(EVR),Code=16"
dbpf $(SYS)-$(DEVICE):EvtG-SP.VAL 16
dbpf $(SYS)-$(DEVICE):EvtH-SP.OUT "@OBJ=$(EVR),Code=17"
dbpf $(SYS)-$(DEVICE):EvtH-SP.VAL 17

# Set up of UnivIO 1 as Input. Generate Code 125 locally on rising edge.
#dbpf $(SYS)-$(DEVICE):OutFPUV01-Src-SP 61
#dbpf $(SYS)-$(DEVICE):In1-Trig-Ext-Sel "Edge"
#dbpf $(SYS)-$(DEVICE):In1-Code-Ext-SP 125

######### OUTPUTS #########
dbpf $(SYS)-$(DEVICE):DlyGen0-Width-SP 2860 #2.86ms
dbpf $(SYS)-$(DEVICE):DlyGen0-Delay-SP 0 #0ms
dbpf $(SYS)-$(DEVICE):DlyGen0-Evt-Trig0-SP 14
dbpf $(SYS)-$(DEVICE):OutFPUV02-Src-SP 0 #Connect to DlyGen-0

# Set up output 3 as a 1 PPS
#dbpf $(SYS)-$(DEVICE):OutFPUV03-Src-SP 41 #Connect to PS1

#Define 14Hz event with a delay of 10ms (WORKING)
dbpf $(SYS)-$(DEVICE):DlyGen1-Width-SP 1000 #1ms
dbpf $(SYS)-$(DEVICE):DlyGen1-Delay-SP #0ms
dbpf $(SYS)-$(DEVICE):DlyGen1-Evt-Trig0-SP 15
dbpf $(SYS)-$(DEVICE):OutFPUV04-Src-SP 1 #Connect to DlyGen-1

# Set up output 5 at 350 Hz
dbpf $(SYS)-$(DEVICE):DlyGen2-Width-SP 1000 #1ms
dbpf $(SYS)-$(DEVICE):DlyGen2-Delay-SP 0 #0ms
dbpf $(SYS)-$(DEVICE):DlyGen2-Evt-Trig0-SP 16
dbpf $(SYS)-$(DEVICE):OutFPUV05-Src-SP 2 #Connect to DlyGen-2

#dbpf $(SYS)-$(DEVICE):DlyGen1-Evt-Trig0-SP 14
#dbpf $(SYS)-$(DEVICE):DlyGen1-Width-SP 2860 #1ms
#dbpf $(SYS)-$(DEVICE):DlyGen1-Delay-SP 0 #0ms
#dbpf $(SYS)-$(DEVICE):OutFPUV03-Src-SP 1 #Connect output2 to DlyGen-1

#Set up delay generator 2 to trigger on event 16
#dbpf $(SYS)-$(DEVICE):DlyGen2-Width-SP 1000 #1ms
#dbpf $(SYS)-$(DEVICE):DlyGen2-Delay-SP 0 #0ms
#dbpf $(SYS)-$(DEVICE):DlyGen2-Evt-Trig0-SP 16

#Set up delay generator 0 to trigger on event 17 and set universal I/O 2
#dbpf $(SYS)-$(DEVICE):DlyGen0-Width-SP 1000 #1ms
#dbpf $(SYS)-$(DEVICE):DlyGen0-Delay-SP 0 #0ms
#dbpf $(SYS)-$(DEVICE):DlyGen0-Evt-Trig0-SP 17
#dbpf $(SYS)-$(DEVICE):OutFPUV02-Src-SP 0 #Connect to DlyGen-0

######## Sequencer #########
# Select trigger source for soft seq 0, trigger source 0, 2 means pulser 2
#dbpf $(SYS)-$(DEVICE):SoftSeq0-TrigSrc-0-Sel 0

# Load sequencer setup
#dbpf $(SYS)-$(DEVICE):SoftSeq0-Load-Cmd 1

# Enable sequencer
#dbpf $(SYS)-$(DEVICE):SoftSeq0-Enable-Cmd 1

#dbpf $(CHIC_SYS)$(CHOP_DRV)01:Freq-SP 28
#dbpf $(CHIC_SYS)$(CHOP_DRV)02:Freq-SP 28
#dbpf $(CHIC_SYS)$(CHOP_DRV)03:Tube-Pos-Delay 10
#dbpf $(CHIC_SYS)$(CHOP_DRV)04:Tube-Pos-Delay 20
# Check that this command is required.
#dbpf $(SYS)-$(DEVICE):RF-Freq 88051900

# Load sequence events and corresponding tick lists
#system "/bin/bash /epics/iocs/cmds/labs-embla-evr/default_evr_seq.sh $(SYS) $(DEVICE)"

# Hints for setting input PVs from client
#caput -a $(SYS)-$(DEVICE):SoftSeq0-EvtCode-SP 4 14 15 16 17
#caput -a $(SYS)-$(DEVICE):SoftSeq0-Timestamp-SP 4 0 1 2 3
#caput -n $(SYS)-$(DEVICE):SoftSeq0-Commit-Cmd 1

######### TIME STAMP #########

#Forward links to esschicTimestampBuffer.template
#dbpf $(SYS)-$(DEVICE):EvtACnt-I.FLNK $(CHIC_SYS)$(CHOP_DRV):TDC
#dbpf $(SYS)-$(DEVICE):EvtECnt-I.FLNK $(CHIC_SYS)$(CHOP_DRV):Ref

#dbpf $(SYS)-$(DEVICE):EvtBCnt-I.FLNK $(CHIC_SYS)$(CHOP_DRV):TDC
#dbpf $(CHIC_SYS)$(CHOP_DRV)01:BPFO.LNK3 $(CHIC_SYS)$(CHOP_DRV):Ref
dbpf $(CHIC_SYS)$(CHOP_DRV)01:Freq-SP 14
dbpf $(CHIC_SYS)$(CHOP_DRV)02:Freq-SP 14
dbpf $(CHIC_SYS)$(CHOP_DRV)03:Freq-SP 14
dbpf $(CHIC_SYS)$(CHOP_DRV)04:Freq-SP 14
#EOF